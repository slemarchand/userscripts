// ==UserScript==
// @name         Liferay JSONWS Improved UI
// @namespace    http://slemarchand.com/
// @version      0.1
// @description  Improve Liferay JSONWS UI
// @author       Sébastien Le Marchand
// @match        *://*/api/jsonws*
// @grant        none
// @updateURL    https://bitbucket.org/slemarchand/userscripts/raw/master/liferay-jsonws-improved-ui.user.js
// @downloadURL  https://bitbucket.org/slemarchand/userscripts/raw/master/liferay-jsonws-improved-ui.user.js

// ==/UserScript==

(function() {
    'use strict';

    $(function() {

        improve();

		Liferay.SPA.app.on('endNavigate', function() {
			improve();
		});

    });

    function improve() {

    	fixDisplay();

        addCopyToClipboardButton();
    }

	function fixDisplay() {

		// Remove unexpect left margin when user is signed in !

		$('body').removeClass('open');

		// Fix company logo size

		var $logo = $('#heading .site-title .logo img');
		$logo.attr('width','256').attr('height','256');

		// Replace current theme (= the same as Guest site) by classic theme

		$('link.lfr-css-file').each(function(i, e) {
			var $cssLink = $(e);

			console.log($cssLink);

			var href = $cssLink.attr('href');
			href = href.replace(/\/o\/[^\/[]*\//,'/o/classic-theme/');
			href = href.replace(/themeId=[^\&]*\&/,'themeId=classic_WAR_classictheme&');

			console.log(href);

			$cssLink.attr('href', href);
		});
	}

	function addCopyToClipboardButton() {

		$('.lfr-api-results .lfr-code-block').each(function(i, e) {
			var $input = $(e);

			var $button = $('<a href="#" style="font-size: 0.8em !important; float: right; text-decoration: none; top: -10px; position: relative">Copy to clipboard</a>');

			$button.insertAfter($input);

			$button.on('click', null, function(event) {

				event.preventDefault();

				copyTextToClipboard($input);

				return false;
			});
		});
	}

	function copyTextToClipboard($element) {

		var text = $element.text();

		var $textArea = $('<textarea>');
		$textArea.text(text);
		$textArea.insertAfter($element);
		$textArea.focus();
		$textArea.select();

		try {
			document.execCommand('copy');
		} catch (err) {
			console.error('Oops, unable to copy', err);
		}

		$textArea.remove();
	}

})();