// ==UserScript==
// @name         Liferay Product Menu Shortcuts
// @namespace    http://slemarchand.com/
// @version      0.1
// @description  Add admin script shortcut
// @author       Sébastien Le Marchand
// @match        *://*/*
// @grant        none
// @updateURL    https://bitbucket.org/slemarchand/userscripts/raw/master/liferay-product-menu-shortcuts.user.js
// @downloadURL  https://bitbucket.org/slemarchand/userscripts/raw/master/liferay-product-menu-shortcuts.user.js

// ==/UserScript==

(function() {
    'use strict';

    const links = [
        ['Users', '/group/control_panel/manage?p_p_id=com_liferay_users_admin_web_portlet_UsersAdminPortlet&p_p_lifecycle=0&p_p_state=maximized'],
        ['Server Admin Script', '/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_com_liferay_server_admin_web_portlet_ServerAdminPortlet_mvcRenderCommandName=%2Fserver_admin%2Fview&_com_liferay_server_admin_web_portlet_ServerAdminPortlet_tabs1=script&_com_liferay_server_admin_web_portlet_ServerAdminPortlet_tabs2='],
        ['App Manager', '/group/control_panel/manage?p_p_id=com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet&p_p_lifecycle=0&p_p_state=maximized']
    ];

	if(jQuery) {
    	jQuery(function() { 
    		if(Liferay && Liferay.SPA) {
                links.forEach(function(link) {
                    addShortcut(link[0], link[1]);
                });
    		}
    	});
    }

    function addShortcut(title,href) {
    	$('#productMenuSidebar .sidebar-body .panel-group').prepend($(
    		'<div class="panel" role="tab"><div class="panel-heading"><div class="panel-title">' 
    		+ '<a href="' + href + '" target="_blank" class="collapse-icon collapse-icon-middle panel-toggler collapsed" role="button">' 
    		+ '<span class="category-name truncate-text">' + title + '</span>'
            + '<span class="collapse-icon-closed">'
            + '<svg class="lexicon-icon lexicon-icon-shortcut" focusable="false" role="img" viewBox="0 0 512 512"> <path class="lexicon-icon-outline" d="M480,320c-17.6729,0-32,14.3271-32,32v96H64V64h96c17.6729,0,32-14.3271,32-32S177.6729,0,160,0H64 C28.7104,0,0,28.7104,0,64v384c0,35.29,28.7104,64,64,64h384c35.29,0,64-28.71,64-64v-96C512,334.3271,497.6729,320,480,320z"></path> <path class="lexicon-icon-outline" d="M480,0H320c-17.6729,0-32,14.3267-32,32s14.3271,32,32,32h82.7456L265.373,201.3726 c-12.4971,12.4971-12.4971,32.7578,0,45.2549C271.6211,252.876,279.8105,256,288,256s16.3789-3.124,22.627-9.3726L448,109.2544V192 c0,17.6733,14.3271,32,32,32s32-14.3267,32-32V32C512,14.3267,497.6729,0,480,0z"></path> </svg>'
            + '</span>'
    		+ '</a></div></div></div>'));
    }
})();